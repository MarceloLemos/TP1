////////////////////////////////////////////////////////////////////////////////
///
/// File:       binarySearch.c
/// Author:     Marcelo Luiz Harry Diniz Lemos
/// Date:       2016-09-30
///
/// Description:
///     Source file for "binarySearch.h". This library is responsible for
///     doing the search on the library.
///
////////////////////////////////////////////////////////////////////////////////

/***************************** INCLUDED LIBRARIES *****************************/

#include <stdio.h>
#include <string.h>

#include "book.h"
#include "binarySearch.h"

/********************************* FUNCTIONS **********************************/

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     This function uses the Binary Search algorithm to find a book on a
///     shelf.
///
/// Parameters:
///     bookName:       title of the desired book
///     shelfNumber:    number of the shelf where the book might be stored
///     position:       position of the book on the shelf
///     available:      flag for the book availability
///     bookExists:     flag to know if the book is present
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void binarySearch(char * bookName, int shelfNumber, int *position, char *available, char *bookExists) {
    FILE * shelf;
    char shelfName[20];
    int left, right;
    bookT * buffer;

    buffer = initializeBook();
    sprintf(shelfName, "estante%d", shelfNumber);
    shelf = fopen(shelfName, "rb");
    left = 0;
    fseek(shelf, 0, SEEK_END);
    right = ftell(shelf) / (MAX_BOOK_TITLE + 2);
    while(left <= right) {
        *position = (left + right) / 2;
        fseek(shelf, (*(position) * (MAX_BOOK_TITLE + 2)), SEEK_SET);
        bookDeserialize(buffer, shelf);
        if(strcmp(bookName, buffer->title) == 0) {
            *available = buffer->available;
            break;
        }
        else if(strcmp(bookName, buffer->title) < 0)
            right = *position - 1;
        else
            left = *position + 1;
    }
    if(left > right)
        *bookExists = 0;
    freeBook(buffer);
    fclose(shelf);
}

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Searches for a book in the library. In the first part a linear search
///     is used to find the number of the shelf where the book might be stored.
///     Then it uses the function "binarySearch" to discover its position on
///     the shelf. If it is in the library and available it prints the shelf and
///     position. If it is in the library but is not available it prints
///     "emprestado" (borrowed). If its not in the library it prints
///     "livro nao encontrado" (book not found).
///
/// Parameters:
///     bookName:   Title of the desired book
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void searchBook(char * bookName) {
    FILE * index;
    char bookExists;
    char available;
    char buffer[MAX_BOOK_TITLE + 1];
    int shelfNumber;
    int position;

    index = fopen("indice", "r");
    bookExists = 1;
    shelfNumber = 0;

    while(1) {
        if(fscanf(index, "%s\n", buffer) == EOF){
            bookExists = 0;
            break;
        }
        if(strcmp(bookName, buffer) < 0 || strcmp(buffer, "#") == 0) {
            bookExists = 0;
            break;
        }
        if(fscanf(index, "%s\n", buffer) == EOF){
            bookExists = 0;
            break;
        }
        if(strcmp(bookName, buffer) <= 0) {
            break;
        }
        shelfNumber++;
    }
    if(bookExists) {
        binarySearch(bookName, shelfNumber, &position, &available, &bookExists);
    }
    if(bookExists) {
        if(available)
            printf("disponivel na posicao %d da estante %d\n", position, shelfNumber);
        else
            printf("emprestado\n");
    } else {
        printf("livro nao encontrado\n");
    }
    fclose(index);
}
