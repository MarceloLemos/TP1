////////////////////////////////////////////////////////////////////////////////
///
/// File:       binarySearch.h
/// Author:     Marcelo Luiz Harry Diniz Lemos
/// Date:       2016-09-30
///
/// Description:
///     Header file for "binarySearch.h". This library is responsible for
///     doing the search on the library.
///
////////////////////////////////////////////////////////////////////////////////

#ifndef _BINARY_SEARCH_H_
#define _BINARY_SEARCH_H_

/********************************* FUNCTIONS **********************************/

////////////////////////////////////////////////////////////////////////////////
///
/// Description:
///     Searches for a book in the library. In the first part a linear search
///     is used to find the number of the shelf where the book might be stored.
///     Then it uses the function "binarySearch" to discover its position on
///     the shelf. If it is in the library and available it prints the shelf and
///     position. If it is in the library but is not available it prints
///     "emprestado" (borrowed). If its not in the library it prints
///     "livro nao encontrado" (book not found).
///
/// Parameters:
///     bookName:   Title of the desired book
///
/// Return:
///     Nothing.
///
////////////////////////////////////////////////////////////////////////////////

void searchBook(char * bookName);

#endif  /* _BINARY_SEARCH_H_ */
